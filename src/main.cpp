#include "Utils.hpp"

#include "window/AppWindow.hpp"

int main() {
  AppWindow window = AppWindow(600, 600, "GumImplicitSurf");
  window.init();
  window.loop();
  window.end();
  return 0;
}
