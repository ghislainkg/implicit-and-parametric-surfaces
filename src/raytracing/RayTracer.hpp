
#ifndef _GUM_IMPLICIT_SURF_RAY_TRACER_
#define _GUM_IMPLICIT_SURF_RAY_TRACER_

#include "../Utils.hpp"
#include "../surfaces/ImplicitSurface.hpp"

enum CameraProjection {
  PERSPECTIVE, ORTHOGRAPHIC
};

class RayTracerCamera {
  friend class DirectRayTracer;
public:
  RayTracerCamera(
    const glm::vec3 & pos = {1,0,0},
    const glm::vec3 & dir = {-1,0,0}
  ):
  pos(pos), direction(glm::normalize(dir)) {}

  inline const glm::vec3 & getPos() const {return pos;}
  inline const glm::vec3 & getDirection() const {return direction;}
  inline void setPos(const glm::vec3 & p) {pos=p;}
  inline void setDirection(const glm::vec3 & d) {direction=glm::normalize(d);}

  // w and h coordinates on the 2d grid
  // left(0) -> right(W-1)
  // top(0) -> bottom(H-1)
  // Returns coordinates centered at the center of the screen
  glm::vec2 toScreenSpace(float w, float h);

  // xpix and hpix the coordinates on screen centered at the center of the screen
  Ray makeRayForPixel(float wpix, float hpix);

  CameraProjection projection = CameraProjection::ORTHOGRAPHIC;
  float perspectiveFocalLength = 0.9f;

  inline int getW() {return W;}
  inline int getH() {return H;}

  float orthoOpeningW = 2.5f;
  float orthoOpeningH = 2.5f;

protected:
  glm::vec3 pos;
  glm::vec3 direction;

  int W = 150;
  int H = 150;

  glm::vec3 UP = {0,1,0};
};

enum DisplayColor {
  NORMAL_VEC, CLOSEST_POINT_COLOR, UNIFORM_COLOR
};

class DirectRayTracer {
public:
  DirectRayTracer(int W, int H) {
    camera.W = W;
    camera.H = H;
    pixelsColor.resize(camera.W*camera.H);
  }

  inline RayTracerCamera & getCamera() {return camera;}

  void computeFrame();

  ImplicitSurface * surface = nullptr;

  void saveFrame(const std::string & filepath);

  inline std::vector<glm::vec3> & getPixelsColor() {return pixelsColor;}

  glm::vec3 lightPos = {2, 2, 1};

  DisplayColor displayColor = DisplayColor::NORMAL_VEC;

private:
  RayTracerCamera camera;

  std::vector<glm::vec3> pixelsColor;
};

#endif
