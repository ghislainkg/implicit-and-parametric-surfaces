#include "RayTracer.hpp"

glm::vec2 RayTracerCamera::toScreenSpace(float w, float h) {
  return {
    2.0f*(float(w)/W) - 1,
    1.0f-2.0f*(float(h)/H)
  };
}

Ray RayTracerCamera::makeRayForPixel(float wpix, float hpix) {
  Ray r;
  glm::vec3 xy = glm::vec3(toScreenSpace(wpix, hpix), 0.0f);
  if(projection==CameraProjection::PERSPECTIVE) {
    r.start = pos;
    r.dir = glm::cross(direction,UP)*xy.x + UP*xy.y + direction*perspectiveFocalLength;
  }
  else if(projection==CameraProjection::ORTHOGRAPHIC) {
    xy = xy * glm::vec3(orthoOpeningW, orthoOpeningH, 1.0);
    r.start = pos + glm::cross(direction,UP)*xy.x + UP*xy.y;
    r.dir = direction;
  }
  r.dir = glm::normalize(r.dir);
  return r;
}

void DirectRayTracer::computeFrame() {
  #pragma omp for
  for(uint w=0; w<camera.W; w++) {
    for(uint h=0; h<camera.H; h++) {
      pixelsColor[w + (camera.H-1-h) * camera.W] = {0,0,0};

      float C = 0;
      float D = 0;
      for(int c=-C; c<=C; c++) {
        for(int d=-D; d<=D; d++) {
          Ray ray = camera.makeRayForPixel(w+c/(C+1e-18),h+d/(D+1e-18));
          if(surface) {
            glm::vec3 intersectionPoint;
            if(surface->findIntersection(ray, intersectionPoint)) {
              glm::vec3 normal = surface->evaluateNormal(intersectionPoint);

              if(displayColor==DisplayColor::CLOSEST_POINT_COLOR) {
                pixelsColor[w + (camera.H-1-h) * camera.W] += 
                  surface->evaluateColor(intersectionPoint);
              }
              else if(displayColor==DisplayColor::NORMAL_VEC) {
                pixelsColor[w + (camera.H-1-h) * camera.W] += normal;
              }
              else if(displayColor==DisplayColor::UNIFORM_COLOR) {
                float dot = std::max(glm::dot(normal, glm::normalize(lightPos-intersectionPoint)), 0.0f);
                pixelsColor[w + (camera.H-1-h) * camera.W] += COLOR_GREEN * dot + COLOR_GREEN*0.1f;
              }
            }
          }
        }
      }

      pixelsColor[w + (camera.H-1-h) * camera.W] *= 1/((2*C+1)*(2*D+1));
    }
  }
}

void DirectRayTracer::saveFrame(const std::string & filepath) {
  FILE *f = fopen(filepath.c_str(), "w");
  fprintf(f, "P3\n%d %d\n%d\n", camera.W, camera.H, 255);

  for (int i=0; i<camera.W*camera.H; i++) {
    fprintf(f,"%d %d %d ",
      (int)(pixelsColor[i].x * 255),
      (int)(pixelsColor[i].y * 255),
      (int)(pixelsColor[i].z * 255)
    );
  }
  fclose(f);
}
