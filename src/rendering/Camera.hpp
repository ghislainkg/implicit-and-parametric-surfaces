#ifndef _GUM_IMPLICIT_SURF_RENDERER_
#define _GUM_IMPLICIT_SURF_RENDERER_

#include "../Utils.hpp"

class Camera {
public:
  Camera() {}

  inline glm::mat4 getProjectionMatrix() const {
    return glm::perspective(
      glm::radians(fov), aspectRatio, near, far
    );
  }

  inline glm::mat4 getViewMatrix() const {
    return glm::lookAt(
      pos, target, up
    );
  }

  inline glm::vec3 getFront() const {return glm::normalize(target-pos);}

  glm::vec3 up = {0,1,0};
  glm::vec3 pos = {3, 0, 0};
  glm::vec3 target = {0, 0, 0};

  float fov = 45.f;
  float aspectRatio = 1.f;
  float near = 0.01f;
  float far = 10.f;
};

#endif
