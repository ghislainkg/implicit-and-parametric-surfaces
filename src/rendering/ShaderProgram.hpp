#ifndef _GUM_IMPLICIT_SURF_SHADER_PROGRAM_
#define _GUM_IMPLICIT_SURF_SHADER_PROGRAM_

#include "../Utils.hpp"

class Program {
public:
  Program() {}

  bool loadShader(
    GLuint program, GLenum type,
    const std::vector<std::string> & shaderFilepaths
  );
  bool initProgram(
    std::vector<std::string> vertexShaderFilenames,
    std::vector<std::string> fragmentShaderFilenames,
    std::vector<std::string> geometryShaderFilenames
  );

  GLuint program = -1;

public:
  void use() const;
};

#ifndef GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR
  #define GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR "./shaders"
#endif

#endif
