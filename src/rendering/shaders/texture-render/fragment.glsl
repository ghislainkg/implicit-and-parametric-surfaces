#version 420 core

out vec4 out_color;

in vec2 texCoord;

uniform sampler2D imageTexture;

void main() {
  out_color = vec4(texture(imageTexture, texCoord).rgb, 1.0);
  // out_color = vec4(0.5, 0.0, 0.0, 1.0);
}
