#version 420 core

layout(location=0) in float splinePatch; // Not used

void main() {
  gl_Position = vec4(splinePatch, 0.1, 0.0, 1.0);
}
