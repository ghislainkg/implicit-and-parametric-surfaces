#version 420 core

// Each point is a cubic spline patch
layout (points) in;
layout (triangle_strip, max_vertices=256) out;

uniform float resolutionU;
uniform float resolutionV;

uniform float polynomialCoefsX[16];
uniform float polynomialCoefsY[16];
uniform float polynomialCoefsZ[16];

uniform mat4 viewMat;
uniform mat4 projMat;

// x, y in [0,1]
void evaluatePolynomial(in float u, in float v, in float polynomialCoefs[16], out float res) {
  res = 0;
  float upow = 0;
  float vpow = 0;
  for(int i=0; i<4; i++) {
    for(int j=0; j<4; j++) {
      if(u==0 && i==0) upow = 1;
      else upow = pow(u,i);

      if(v==0 && j==0) vpow = 1;
      else vpow = pow(v,j);

      res = res + polynomialCoefs[4*i+j]*upow*vpow;
    }
  }
}

vec4 pos00 = vec4(0,0,0, 1);
vec4 pos01 = vec4(0,0,0, 1);
vec4 pos10 = vec4(0,0,0, 1);
vec4 pos11 = vec4(0,0,0, 1);
void handlePoint(in float u0, in float u1, in float v0, in float v1) {
  evaluatePolynomial(u0, v0, polynomialCoefsX, pos00.x);
  evaluatePolynomial(u0, v0, polynomialCoefsY, pos00.y);
  evaluatePolynomial(u0, v0, polynomialCoefsZ, pos00.z);

  evaluatePolynomial(u0, v1, polynomialCoefsX, pos01.x);
  evaluatePolynomial(u0, v1, polynomialCoefsY, pos01.y);
  evaluatePolynomial(u0, v1, polynomialCoefsZ, pos01.z);

  evaluatePolynomial(u1, v0, polynomialCoefsX, pos10.x);
  evaluatePolynomial(u1, v0, polynomialCoefsY, pos10.y);
  evaluatePolynomial(u1, v0, polynomialCoefsZ, pos10.z);

  evaluatePolynomial(u1, v1, polynomialCoefsX, pos11.x);
  evaluatePolynomial(u1, v1, polynomialCoefsY, pos11.y);
  evaluatePolynomial(u1, v1, polynomialCoefsZ, pos11.z);

  gl_Position = projMat*viewMat*pos00;
  EmitVertex();
  gl_Position = projMat*viewMat*pos01;
  EmitVertex();
  gl_Position = projMat*viewMat*pos11;
  EmitVertex();
  EndPrimitive();

  gl_Position = projMat*viewMat*pos00;
  EmitVertex();
  gl_Position = projMat*viewMat*pos11;
  EmitVertex();
  gl_Position = projMat*viewMat*pos10;
  EmitVertex();
  EndPrimitive();
}

void main() {
  float u0 = 0.0;
  float v0 = 0.0;
  float u1 = 0.0;
  float v1 = 0.0;
  float resolution = 6;
  for(int i=0; i<resolution; i++) {
    for(int j=0; j<resolution; j++) {
      u0 = i/resolution;
      v0 = j/resolution;
      u1 = (i+1)/resolution;
      v1 = (j+1)/resolution;

      handlePoint(u0,u1,v0,v1);
    }
  }
}
