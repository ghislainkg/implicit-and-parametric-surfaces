#include "ShaderProgram.hpp"


static void showSources(const std::list<std::string> & sources)  {
  uint lineIndex = 1;
  for(auto & source : sources) {
    std::stringstream stream = std::stringstream(source);
    std::string line;
    while(std::getline(stream, line)) {
      std::cout << lineIndex << " " << line << std::endl;
      lineIndex += 1;
    }
  }
}

bool Program::loadShader(
  GLuint program, GLenum type,
  const std::vector<std::string> & shaderFilepaths
) {
  if(shaderFilepaths.size()==0) return true;

  GLuint shader = glCreateShader(type);

  GLchar * shaderSources[shaderFilepaths.size()];
  std::list<std::string> sources;
  int i=0;
  for(auto & shaderFilepath : shaderFilepaths) {
    sources.push_back(file2String(shaderFilepath));
    GLchar *shaderSource = (GLchar *)sources.back().c_str();
    shaderSources[i] = shaderSource;
    i++;
  }

  glShaderSource(shader, shaderFilepaths.size(), shaderSources, NULL);
  glCompileShader(shader);
  GLint success;
  GLchar infoLog[512];
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  if(!success) {
    glGetShaderInfoLog(shader, 512, NULL, infoLog);
    std::cerr << "ERROR in compiling shader";
    for(auto & shaderFilepath : shaderFilepaths) {
      std::cerr << "\"" << shaderFilepath << "\" ";
    }
    std::cerr << "\n\t" << infoLog << std::endl;

    std::cout << std::endl << std::endl;
    showSources(sources);
    std::cout << std::endl << std::endl;

    assert(false);

    return false;
  }
  glAttachShader(program, shader);
  glDeleteShader(shader);
  getOpenGLError("Program::loadShader()");
  return true;
}
bool Program::initProgram(
  std::vector<std::string> vertexShaderFilenames,
  std::vector<std::string> fragmentShaderFilenames,
  std::vector<std::string> geometryShaderFilenames
) {
  program = glCreateProgram();
  if(!loadShader(program, GL_VERTEX_SHADER, vertexShaderFilenames))
    return false;
  if(!loadShader(program, GL_FRAGMENT_SHADER, fragmentShaderFilenames))
    return false;
  if(!loadShader(program, GL_GEOMETRY_SHADER, geometryShaderFilenames))
    return false;
  glLinkProgram(program);

  GLint success;
  GLchar infoLog[512];
  glGetProgramiv(program, GL_LINK_STATUS, &success);
  if(!success) {
    glGetProgramInfoLog(program, 512, NULL, infoLog);
    std::cerr << "\n" << infoLog << std::endl;
    return false;
  }

  getOpenGLError("Program::initProgram()");
  return true;
}
void Program::use() const {
  glUseProgram(program);
  getOpenGLError("Program::use()");
}
