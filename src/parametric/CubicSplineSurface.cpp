#include "CubicSplineSurface.hpp"

#include <Eigen/Dense>

void BicubicInterpolation::update() {
  Eigen::MatrixXd B1 = Eigen::MatrixXd(4, 4);
  Eigen::MatrixXd B2 = Eigen::MatrixXd(4, 4);
  Eigen::MatrixXd P = Eigen::MatrixXd(4, 4);
  Eigen::MatrixXd A = Eigen::MatrixXd(4, 4);

  B1(0,0) = 1; B1(0,1) = 0; B1(0,2) = 0; B1(0,3) = 0;
  B1(1,0) = 0; B1(1,1) = 0; B1(1,2) = 1; B1(1,3) = 0;
  B1(2,0) = -3; B1(2,1) = 3; B1(2,2) = -2; B1(2,3) = -1;
  B1(3,0) = 2; B1(3,1) = -2; B1(3,2) = 1; B1(3,3) = 1;

  B2(0,0) = 1; B2(0,1) = 0; B2(0,2) = -3; B2(0,3) = 2;
  B2(1,0) = 0; B2(1,1) = 0; B2(1,2) = 3; B2(1,3) = -2;
  B2(2,0) = 0; B2(2,1) = 1; B2(2,2) = -2; B2(2,3) = 1;
  B2(3,0) = 0; B2(3,1) = 0; B2(3,2) = -1; B2(3,3) = 1;

  P(0,0) = p00; P(0,1) = p01; P(0,2) = py00; P(0,3) = py01;
  P(1,0) = p10; P(1,1) = p11; P(1,2) = py10; P(1,3) = py11;
  P(2,0) = px00; P(2,1) = px01; P(2,2) = pxy00; P(2,3) = pxy01;
  P(3,0) = px10; P(3,1) = px11; P(3,2) = pxy10; P(3,3) = pxy11;

  A = B1 * P * B2;
  aij[0][0] = A(0,0); aij[0][1] = A(0,1); aij[0][2] = A(0,2); aij[0][3] = A(0,3);
  aij[1][0] = A(1,0); aij[1][1] = A(1,1); aij[1][2] = A(1,2); aij[1][3] = A(1,3);
  aij[2][0] = A(2,0); aij[2][1] = A(2,1); aij[2][2] = A(2,2); aij[2][3] = A(2,3);
  aij[3][0] = A(3,0); aij[3][1] = A(3,1); aij[3][2] = A(3,2); aij[3][3] = A(3,3);

  // std::cout << A << std::endl;
  // std::cout << "interpolate (0.5, 0.5) = " << interpolateP(0.5f, 0.5f) << std::endl;
  // std::cout << "interpolate (0, 0) = " << interpolateP(0.0f, 0.0f) << std::endl;
  // std::cout << "interpolate (0, 1) = " << interpolateP(0.0f, 1.0f) << std::endl;
  // std::cout << "interpolate (1, 0) = " << interpolateP(1.0f, 0.0f) << std::endl;
  // std::cout << "interpolate (1, 1) = " << interpolateP(1.0f, 1.0f) << std::endl;
}

float BicubicInterpolation::interpolateP(float x, float y) {
  float res = 0;
  for(uint i=0; i<4; i++) {
    for(uint j=0; j<4; j++) {
      res += aij[i][j]*std::pow(x,i)*std::pow(y,j);
    }
  }
  return res;
}

void CubicSplineSurfacePatch::setPoint00(const glm::vec3 & p) {
  interpolationX.p00 = p.x;
  interpolationY.p00 = p.y;
  interpolationZ.p00 = p.z;
}
void CubicSplineSurfacePatch::setPoint01(const glm::vec3 & p) {
  interpolationX.p01 = p.x;
  interpolationY.p01 = p.y;
  interpolationZ.p01 = p.z;
}
void CubicSplineSurfacePatch::setPoint10(const glm::vec3 & p) {
  interpolationX.p10 = p.x;
  interpolationY.p10 = p.y;
  interpolationZ.p10 = p.z;
}
void CubicSplineSurfacePatch::setPoint11(const glm::vec3 & p) {
  interpolationX.p11 = p.x;
  interpolationY.p11 = p.y;
  interpolationZ.p11 = p.z;
}

void CubicSplineSurfacePatch::setTangentPlane00(const glm::vec3 & v1, const glm::vec3 & v2) {
  // Tangent plane slope for X variable
  interpolationX.px00 = v1.x;
  interpolationX.py00 = v2.x;
  interpolationX.pxy00 = v1.x + v2.x;
  // Tangent plane slope for Y variable
  interpolationY.px00 = v1.y;
  interpolationY.py00 = v2.y;
  interpolationY.pxy00 = v1.y + v2.y;
  // Tangent plane slope for Z variable
  interpolationZ.px00 = v1.z;
  interpolationZ.py00 = v2.z;
  interpolationZ.pxy00 = v1.z + v2.z;
}
void CubicSplineSurfacePatch::setTangentPlane01(const glm::vec3 & v1, const glm::vec3 & v2) {
  // Tangent plane slope for X variable
  interpolationX.px01 = v1.x;
  interpolationX.py01 = v2.x;
  interpolationX.pxy01 = v1.x + v2.x;
  // Tangent plane slope for Y variable
  interpolationY.px01 = v1.y;
  interpolationY.py01 = v2.y;
  interpolationY.pxy01 = v1.y + v2.y;
  // Tangent plane slope for Z variable
  interpolationZ.px01 = v1.z;
  interpolationZ.py01 = v2.z;
  interpolationZ.pxy01 = v1.z + v2.z;
}
void CubicSplineSurfacePatch::setTangentPlane10(const glm::vec3 & v1, const glm::vec3 & v2) {
  // Tangent plane slope for X variable
  interpolationX.px10 = v1.x;
  interpolationX.py10 = v2.x;
  interpolationX.pxy10 = v1.x + v2.x;
  // Tangent plane slope for Y variable
  interpolationY.px10 = v1.y;
  interpolationY.py10 = v2.y;
  interpolationY.pxy10 = v1.y + v2.y;
  // Tangent plane slope for Z variable
  interpolationZ.px10 = v1.z;
  interpolationZ.py10 = v2.z;
  interpolationZ.pxy10 = v1.z + v2.z;
}
void CubicSplineSurfacePatch::setTangentPlane11(const glm::vec3 & v1, const glm::vec3 & v2) {
  // Tangent plane slope for X variable
  interpolationX.px11 = v1.x;
  interpolationX.py11 = v2.x;
  interpolationX.pxy11 = v1.x + v2.x;
  // Tangent plane slope for Y variable
  interpolationY.px11 = v1.y;
  interpolationY.py11 = v2.y;
  interpolationY.pxy11 = v1.y + v2.y;
  // Tangent plane slope for Z variable
  interpolationZ.px11 = v1.z;
  interpolationZ.py11 = v2.z;
  interpolationZ.pxy11 = v1.z + v2.z;
}

glm::vec3 CubicSplineSurfacePatch::getPoint00() {
  return glm::vec3(
    interpolationX.p00,
    interpolationY.p00,
    interpolationZ.p00
  );
}
glm::vec3 CubicSplineSurfacePatch::getPoint01() {
  return glm::vec3(
    interpolationX.p01,
    interpolationY.p01,
    interpolationZ.p01
  );
}
glm::vec3 CubicSplineSurfacePatch::getPoint10() {
  return glm::vec3(
    interpolationX.p10,
    interpolationY.p10,
    interpolationZ.p10
  );
}
glm::vec3 CubicSplineSurfacePatch::getPoint11() {
  return glm::vec3(
    interpolationX.p11,
    interpolationY.p11,
    interpolationZ.p11
  );
}

std::pair<glm::vec3, glm::vec3> CubicSplineSurfacePatch::getTangentPlane00() {
  return {
    {
      interpolationX.px00,
      interpolationY.px00,
      interpolationZ.px00
    },
    {
      interpolationX.py00,
      interpolationY.py00,
      interpolationZ.py00
    }
  };
}
std::pair<glm::vec3, glm::vec3> CubicSplineSurfacePatch::getTangentPlane01() {
  return {
    {
      interpolationX.px01,
      interpolationY.px01,
      interpolationZ.px01
    },
    {
      interpolationX.py01,
      interpolationY.py01,
      interpolationZ.py01
    }
  };
}
std::pair<glm::vec3, glm::vec3> CubicSplineSurfacePatch::getTangentPlane10() {
  return {
    {
      interpolationX.px10,
      interpolationY.px10,
      interpolationZ.px10
    },
    {
      interpolationX.py10,
      interpolationY.py10,
      interpolationZ.py10
    }
  };
}
std::pair<glm::vec3, glm::vec3> CubicSplineSurfacePatch::getTangentPlane11() {
  return {
    {
      interpolationX.px11,
      interpolationY.px11,
      interpolationZ.px11
    },
    {
      interpolationX.py11,
      interpolationY.py11,
      interpolationZ.py11
    }
  };
}

void CubicSplineSurfacePatch::update() {
  interpolationX.update();
  interpolationY.update();
  interpolationZ.update();
}
