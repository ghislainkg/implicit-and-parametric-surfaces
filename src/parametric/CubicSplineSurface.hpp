
#ifndef _GUM_IMPLICIT_SURF_CUBIC_SPLINE_SURFACE_
#define _GUM_IMPLICIT_SURF_CUBIC_SPLINE_SURFACE_

#include "../Utils.hpp"

class BicubicInterpolation {
  friend class CubicSplineSurfacePatch;
public:
  BicubicInterpolation() {}

  /*x, y in [0,1]*/
  float interpolateP(float x, float y);

  inline std::vector<float> getPolynomialCoefs() {
    return {
      aij[0][0], aij[0][1], aij[0][2], aij[0][3],
      aij[1][0], aij[1][1], aij[1][2], aij[1][3],
      aij[2][0], aij[2][1], aij[2][2], aij[2][3],
      aij[3][0], aij[3][1], aij[3][2], aij[3][3]
    };
  }

protected:

  void update();

  // Values at boundaries
  float p00 = 0;
  float p01 = 0;
  float p10 = 0;
  float p11 = 0;

  // X partial derivative at boundaries
  float px00 = 0;
  float px01 = 0;
  float px10 = 0;
  float px11 = 0;

  // Y partial derivative at boundaries
  float py00 = 0;
  float py01 = 0;
  float py10 = 0;
  float py11 = 0;

  // XY partial derivative at boundaries
  float pxy00 = 0;
  float pxy01 = 0;
  float pxy10 = 0;
  float pxy11 = 0;

  // Coefficients of the 2 variable cubic polynomial
  float aij[4][4] = {};
};

class CubicSplineSurfacePatch {
public:
  CubicSplineSurfacePatch() {}

  void setPoint00(const glm::vec3 & p);
  void setPoint01(const glm::vec3 & p);
  void setPoint10(const glm::vec3 & p);
  void setPoint11(const glm::vec3 & p);

  void setTangentPlane00(const glm::vec3 & v1, const glm::vec3 & v2);
  void setTangentPlane01(const glm::vec3 & v1, const glm::vec3 & v2);
  void setTangentPlane10(const glm::vec3 & v1, const glm::vec3 & v2);
  void setTangentPlane11(const glm::vec3 & v1, const glm::vec3 & v2);

  glm::vec3 getPoint00();
  glm::vec3 getPoint01();
  glm::vec3 getPoint10();
  glm::vec3 getPoint11();

  std::pair<glm::vec3, glm::vec3> getTangentPlane00();
  std::pair<glm::vec3, glm::vec3> getTangentPlane01();
  std::pair<glm::vec3, glm::vec3> getTangentPlane10();
  std::pair<glm::vec3, glm::vec3> getTangentPlane11();

  void update();

  BicubicInterpolation interpolationX;
  BicubicInterpolation interpolationY;
  BicubicInterpolation interpolationZ;
};

class CubicSplineSurface {
public:
  CubicSplineSurface() {}

private:
  std::vector<glm::vec3> points;
};

#endif
