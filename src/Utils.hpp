#ifndef _GUM_IMPLICIT_SURF_
#define _GUM_IMPLICIT_SURF_

// GLM (for maths)
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/matrix_decompose.hpp>

// C++ STD library
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <string>
#include <cmath>
#include <memory>
#include <algorithm>
#include <functional>
#include <type_traits>

// Glad (for OpenGL functions)
#include <glad/glad.h>

// GLWF (for OpenGL context and window)
#include <GLFW/glfw3.h>

// ImGui
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#define getOpenGLError(where) {int e = glGetError();if(e != GL_NO_ERROR) {std::cout << "GL Error detected : " << where << " : " << e << std::endl;assert(false);}}

inline std::ostream & operator<<(std::ostream & out, const glm::mat3 & m) {
  out << "|" << m[0][0] << "  " << m[1][0] << "  " << m[2][0] << "|" << std::endl;
  out << "|" << m[0][1] << "  " << m[1][1] << "  " << m[2][1] << "|" << std::endl;
  out << "|" << m[0][2] << "  " << m[1][2] << "  " << m[2][2] << "|" << std::endl;
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::mat4 & m) {
  out << "|" << m[0][0] << "  " << m[1][0] << "  " << m[2][0] << m[3][0] << "|" << std::endl;
  out << "|" << m[0][1] << "  " << m[1][1] << "  " << m[2][1] << m[3][1] << "|" << std::endl;
  out << "|" << m[0][2] << "  " << m[1][2] << "  " << m[2][2] << m[3][2] << "|" << std::endl;
  out << "|" << m[0][3] << "  " << m[1][3] << "  " << m[2][3] << m[3][3] << "|" << std::endl;
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec4 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << "  " << v.w << ")";
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec3 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << ")";
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec2 & v) {
  out << "(" << v.x << "  " << v.y << ")";
  return out;
}

inline std::string file2String(const std::string &filename) {
  std::ifstream t(filename.c_str());
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}

#define COLOR_RED glm::vec3(0.71f, 0.082f, 0.035f)
#define COLOR_GREEN glm::vec3(0.165f, 0.71f, 0.035f)
#define COLOR_BLUE glm::vec3(0.106f, 0.322f, 0.859f)
#define COLOR_YELLOW glm::vec3(0.859f, 0.835f, 0.106f)
#define COLOR_GRAY glm::vec3(0.388f, 0.388f, 0.388f)
#define COLOR_BLACK glm::vec3(0.0f, 0.0f, 0.0f)
#define COLOR_WHITE glm::vec3(1.0f, 1.0f, 1.0f)
#define COLOR_ORANGE glm::vec3(1.0f, 0.549f, 0.0f)
#define COLOR_PURPLE glm::vec3(0.463f, 0.0f, 1.0f)
#define COLOR_PINK glm::vec3(1.0f, 0.0f, 1.0f)

struct Ray {
  glm::vec3 start;
  glm::vec3 dir;

};

inline std::ostream & operator<<(std::ostream & out, const Ray & r) {
  out << "Ray: start=" << r.start << " direction=" << r.dir;
  return out;
}

inline glm::vec3 projectPointOnLine(
  const glm::vec3 & point,
  const glm::vec3 & lineA, const glm::vec3 & lineB
) {
  if(lineA==lineB) return lineA;
  glm::vec3 u = lineB - lineA;
  glm::vec3 v = point - lineA;
  float cos = glm::dot(glm::normalize(u), glm::normalize(v));
  return glm::length(v)*cos*u + lineA;
}

#endif
