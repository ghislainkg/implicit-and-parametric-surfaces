#ifndef _GUM_IMPLICIT_SURF_IMPLICIT_SURFACE_
#define _GUM_IMPLICIT_SURF_IMPLICIT_SURFACE_

#include "../Utils.hpp"

enum ImplicitSurfaceType {
  POINT, POLYNOMIAL
};

class ImplicitSurface {
public:

  virtual ImplicitSurfaceType getType() const = 0;

  virtual float evaluate(const glm::vec3 & X) const = 0;
  virtual glm::vec3 evaluateColor(const glm::vec3 & X) const = 0;

  virtual float evaluateDerivX(const glm::vec3 & X) const;
  virtual float evaluateDerivY(const glm::vec3 & X) const;
  virtual float evaluateDerivZ(const glm::vec3 & X) const;
  virtual glm::vec3 evaluateGradient(const glm::vec3 & X) const;
  virtual glm::vec3 evaluateNormal(const glm::vec3 & X) const;
  virtual glm::vec3 evaluateTangent(const glm::vec3 & X) const;
  virtual glm::vec3 evaluateCurvature(const glm::vec3 & X) const;
  virtual glm::vec3 evaluateTorsion(const glm::vec3 & X) const;

  inline void setTranslate(const glm::vec3 & vec) {translationVec=vec;}
  virtual void setRotation(const glm::mat4 & mat) {rotationMatrix = mat;}

  bool findIntersection(const Ray & ray, glm::vec3 & p) const;
  bool hasIntersection(const Ray & ray) const;

  inline void setIsoValue(float v) {isoValue=v;}
  inline float getIsoValue() {return isoValue;}

protected:
  float isoValue = 0.5f;
  glm::vec3 translationVec = {0,0,0};
  glm::mat4 rotationMatrix = glm::mat4(1);
};

#endif
