#include "ImplicitSurface.hpp"

float ImplicitSurface::evaluateDerivX(const glm::vec3 & X) const {
  return evaluate(X-glm::vec3(1e-5,0,0)) - evaluate(X);
}
float ImplicitSurface::evaluateDerivY(const glm::vec3 & X) const {
  return evaluate(X-glm::vec3(0,1e-5,0)) - evaluate(X);
}
float ImplicitSurface::evaluateDerivZ(const glm::vec3 & X) const {
  return evaluate(X-glm::vec3(0,0,1e-5)) - evaluate(X);
}

glm::vec3 ImplicitSurface::evaluateGradient(const glm::vec3 & X) const {
  return {
    evaluateDerivX(X),
    evaluateDerivY(X),
    evaluateDerivZ(X)
  };
}

glm::vec3 ImplicitSurface::evaluateNormal(const glm::vec3 & X) const {
  return glm::normalize(evaluateGradient(X));
}
glm::vec3 ImplicitSurface::evaluateTangent(const glm::vec3 & X) const {
  // TODO
  return glm::vec3();
}
glm::vec3 ImplicitSurface::evaluateCurvature(const glm::vec3 & X) const {
  // TODO
  return glm::vec3();
}
glm::vec3 ImplicitSurface::evaluateTorsion(const glm::vec3 & X) const {
  // TODO
  return glm::vec3();
}

bool ImplicitSurface::findIntersection(const Ray & ray, glm::vec3 & inter) const {
  inter = ray.start;
  float speed = 0.1f;
  bool isIn = false;
  bool enteredVolume = false;
  for(uint i=0; i<20; i++) {
    float val = evaluate(inter);

    if(!isIn && val>=isoValue) {
      // The ray is outside and enters the volume
      isIn = true;
      speed = -(speed*speed); // We try to go back outise at lower speed
    }
    if(val<isoValue && isIn) {
      // The ray is inside and exit the volume
      isIn = false;
      speed = speed*speed; // We try to go back inside at lower speed
    }

    if(!enteredVolume && val>=isoValue) {
      // The ray enter the volume
      enteredVolume = true;
    }

    if(std::abs(val-isoValue)<1e-6) return true;

    inter = inter + ray.dir*speed;
  }

  return enteredVolume;
}

bool ImplicitSurface::hasIntersection(const Ray & ray) const {
  glm::vec3 inter = ray.start;
  float speed = 0.1f;
  for(uint i=0; i<20; i++) {
    float val = evaluate(inter);

    if(val>=isoValue) {
      // The ray enter the volume
      return true;
    }

    if(std::abs(val-isoValue)<1e-6) return true;

    inter = inter + ray.dir*speed;
  }

  return false;
}
