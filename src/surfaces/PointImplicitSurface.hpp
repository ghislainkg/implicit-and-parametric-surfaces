
#ifndef _GUM_IMPLICIT_SURF_POINT_IMPLICIT_SURFACE_
#define _GUM_IMPLICIT_SURF_POINT_IMPLICIT_SURFACE_

#include "ImplicitSurface.hpp"

enum Assembling {
  UNION, INTERSECTION, BLENDING
};

class PointImplicitSurface:
  public ImplicitSurface {
public:
  PointImplicitSurface() {}

  inline ImplicitSurfaceType getType() const {return ImplicitSurfaceType::POINT;}

  float evaluate(const glm::vec3 & X) const override;
  float densityFunction(float distance) const;
  glm::vec3 evaluateColor(const glm::vec3 & X) const override;

  std::list<glm::vec3> points;

  Assembling assembling = Assembling::BLENDING;

private:

  static const std::vector<glm::vec3> COLORS; 
};

#endif
