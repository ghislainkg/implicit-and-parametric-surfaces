#include "PointImplicitSurface.hpp"

const std::vector<glm::vec3> PointImplicitSurface::COLORS = {
  COLOR_WHITE,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_GREEN,
  COLOR_ORANGE,
  COLOR_PINK,
  COLOR_PURPLE,
  COLOR_YELLOW,
  COLOR_RED
};

float PointImplicitSurface::evaluate(const glm::vec3 & X) const {
  float res = 0;

  // TODO : Translate and rotate

  if(assembling==Assembling::BLENDING) res = 0;
  if(assembling==Assembling::UNION) res = 0;
  if(assembling==Assembling::INTERSECTION) res = FLT_MAX;

  float N = points.size();
  for(auto & point : points) {
    if(assembling==Assembling::BLENDING) {
      // Sum
      res += densityFunction(glm::length(point-X));
    }

    if(assembling==Assembling::UNION) {
      float r = densityFunction(glm::length(point-X));
      // Maximum
      if(res < r) {
        res = r;
      }
    }

    if(assembling==Assembling::INTERSECTION) {
      float r = densityFunction(glm::length(point-X));
      // Minimum
      if(res > r) {
        res = r;
      }
    }
  }
  return res;
}

glm::vec3 PointImplicitSurface::evaluateColor(const glm::vec3 & X) const {
  if(points.size()==0) return COLOR_BLACK;
  float dist = glm::length(points.front()-X);
  int indexClosest = 0;
  int i=0;
  for(auto & point : points) {
    if(glm::length(point-X) < dist) {
      dist = glm::length(point-X);
      indexClosest = i;
    }
    i++;
  }
  return COLORS[indexClosest%COLORS.size()];
}

float PointImplicitSurface::densityFunction(float distance) const {
  float normalizedDistance = distance/5;
  return std::exp(-distance*distance);
  // return std::pow(1-normalizedDistance*normalizedDistance, 3);
}
