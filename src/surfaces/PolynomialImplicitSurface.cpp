
#include "PolynomialImplicitSurface.hpp"

float PolynomialImplicitSurface::evaluate(const glm::vec3 & X) const {
  float res = 0;

  // TODO : Translate and rotate

  for(auto & f : factors) {
    res += f.coef*(
      std::pow(X.x, f.degreeX)*
      std::pow(X.y, f.degreeY)*
      std::pow(X.z, f.degreeZ)
    );
  }
  return 1/(res+1e-15);
}

glm::vec3 PolynomialImplicitSurface::evaluateColor(const glm::vec3 & X) const {
  return COLOR_ORANGE;
}
