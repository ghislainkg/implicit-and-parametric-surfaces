
#ifndef _GUM_IMPLICIT_SURF_POLYNOMIAL_IMPLICIT_SURFACE_
#define _GUM_IMPLICIT_SURF_POLYNOMIAL_IMPLICIT_SURFACE_

#include "ImplicitSurface.hpp"

struct PolynomialFactor {
  float coef;
  int degreeX; int degreeY; int degreeZ;
};

class PolynomialImplicitSurface:
  public ImplicitSurface {
public:
  PolynomialImplicitSurface() {
    makeSphere();
  }

  inline ImplicitSurfaceType getType() const override {return ImplicitSurfaceType::POLYNOMIAL;}

  float evaluate(const glm::vec3 & X) const override;
  glm::vec3 evaluateColor(const glm::vec3 & X) const override;

  inline void makeSphere() {
    factors = {
      {1.0f, 2, 0, 0},
      {1.0f, 0, 2, 0},
      {1.0f, 0, 0, 2}
    };
  }

  std::vector<PolynomialFactor> factors;
};

#endif
