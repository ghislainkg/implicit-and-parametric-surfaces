#ifndef _GUM_IMPLICIT_SURF_APP_WINDOW_
#define _GUM_IMPLICIT_SURF_APP_WINDOW_

#include "../Utils.hpp"
#include "Window.hpp"
#include "../raytracing/RayTracer.hpp"
#include "../rendering/ShaderProgram.hpp"
#include "../rendering/Camera.hpp"

#include "../surfaces/PointImplicitSurface.hpp"
#include "../surfaces/PolynomialImplicitSurface.hpp"

#include "../parametric/CubicSplineSurface.hpp"

class AppWindow: public Window {
public:
  AppWindow(
    int width, int height, std::string name
  )
  : Window(width, height, name),
  tracer(100, 100) {}

protected:
  bool drawWire = false;

  void afterInit() override;
  void render(float deltaTime) override;
  void renderImGui(float deltaTime) override;

  void onKeyDown(KeyCode key, KeyDevice device) override;
  void onKeyPush(KeyCode key, KeyDevice device) override;
  void onMouseMove(float x, float y) override;
  void onMouseScroll(float y) override;

  glm::vec2 lastMousePos = {0,0};

// General
  void renderCameraImGui();

// Implicit surface
  ImplicitSurface * surface = nullptr;
  int selectedPointIndex = -1;
  DirectRayTracer tracer;
  GLuint implicit_textureID;
  Program implicit_program;
  GLuint implicit_VAO = 0;
  GLuint implicit_vertices_VBO = 0;
  GLuint implicit_texCoords_VBO = 0;
  void initImplicit();
  void updateImplicitTracerFrame();
  void renderImplicit();
  void renderImplicitImgui();

// Parametric surface
  CubicSplineSurfacePatch * cubicSplinePatch = nullptr;
  Program cubicSplineProgram;
  GLuint cubicSplinePatchVAO = 0;
  GLuint cubicSplinePatch_VBO = 0;
  Camera camera;
  void initCubicSplinePatch();
  void renderCubicSplinePatch();
  void renderCubicSplinePatchImGui();

};

#endif
