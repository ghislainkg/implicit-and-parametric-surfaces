#include "Window.hpp"

static void errorCallback(int error, const char *desc) {
  std::cout <<  "Error " << error << ": " << desc << std::endl;
}

void Window::initGLFW() {
  glfwSetErrorCallback(errorCallback);

  // Initialize GLFW, the library responsible for window management
  if(!glfwInit()) {
    std::cerr << "ERROR: Failed to init GLFW" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Before creating the window, set some option flags
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

  // Create the window
  window = glfwCreateWindow(
    width, height,
    name.c_str(),
    nullptr, nullptr);
  if(!window) {
    std::cerr << "ERROR: Failed to open window" << std::endl;
    glfwTerminate();
    std::exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);

  initKeyListeners();
}

void Window::initOpenGL() {
  if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cerr << "ERROR: Failed to initialize OpenGL context" << std::endl;
    glfwTerminate();
    std::exit(EXIT_FAILURE);
  }
}

void Window::endWindow() {
  clearKeyListeners();
  glfwDestroyWindow(window);
  glfwTerminate();
}

void Window::initImGui() {
  const char* glsl_version = "#version 420";

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
  io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;         // Enable Docking
  io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;       // Enable Multi-Viewport / Platform Windows
  //io.ConfigViewportsNoAutoMerge = true;
  //io.ConfigViewportsNoTaskBarIcon = true;

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  //ImGui::StyleColorsLight();

  // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
  ImGuiStyle& style = ImGui::GetStyle();
  if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
  {
      style.WindowRounding = 0.0f;
      style.Colors[ImGuiCol_WindowBg].w = 1.0f;
  }

  // Setup Platform/Renderer backends
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);
}

void Window::clearImGui() {
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
}

void Window::loop() {
  while(!glfwWindowShouldClose(window)) {
    glfwSwapBuffers(window);

    float currentTime = glfwGetTime();
    float deltaTime = currentTime-lastTime;
    lastTime = currentTime;

    render(deltaTime);
    renderImGui(deltaTime);

    glfwPollEvents();
    processKeys(deltaTime);
  }
}

typedef void (*KeyCallback)(KeyCode h);

struct KeyListener {
  KeyCode key;
  int glfwKeyCode;
  Window * window;
  KeyDevice device;

  float up_press_time=0;

  KeyListener(
    Window * window,
    KeyCode key,
    int glfwKeyCode,
    KeyDevice device
  ): window(window), key(key),
  glfwKeyCode(glfwKeyCode), device(device) {}

  inline virtual void listen(
    float deltaTime
  ) {
    int state;
    if(device==KeyDevice::KEYBOARD) {
      state = glfwGetKey(window->window, glfwKeyCode);
    }
    else if(device==KeyDevice::MOUSE) {
      state = glfwGetMouseButton(window->window, glfwKeyCode);
    }
    if(state == GLFW_PRESS) {
      window->onKeyDown(key, device);
      up_press_time += deltaTime;
    }
    else if(state == GLFW_RELEASE) {
      if(up_press_time>0 && up_press_time<window->keyPushDelay) {
        window->onKeyPush(key, device);
      }
      up_press_time = 0;
    }
  }
};

static std::list<KeyListener> keyListeners;

struct EventListener {
  std::list<Window *> listeners;

  void notifyScroll(float y) {
    for(auto w : listeners) {
      w->onMouseScroll(y);
    }
  }
};
static EventListener staticEventListener;

void Window::initKeyListeners() {
  keyListeners.push_back(KeyListener(this, KeyCode::UP,               GLFW_KEY_UP,                KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::DOWN,             GLFW_KEY_DOWN,              KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::LEFT,             GLFW_KEY_LEFT,              KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::RIGHT,            GLFW_KEY_RIGHT,             KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::SPACE,            GLFW_KEY_SPACE,             KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::CTRL_LEFT,        GLFW_KEY_LEFT_CONTROL,      KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::CTRL_RIGHT,       GLFW_KEY_RIGHT_CONTROL,     KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::Q,                GLFW_KEY_Q,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::W,                GLFW_KEY_W,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::E,                GLFW_KEY_E,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::A,                GLFW_KEY_A,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::S,                GLFW_KEY_S,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::D,                GLFW_KEY_D,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::Z,                GLFW_KEY_Z,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::X,                GLFW_KEY_X,                 KeyDevice::KEYBOARD));
  keyListeners.push_back(KeyListener(this, KeyCode::C,                GLFW_KEY_C,                 KeyDevice::KEYBOARD));

  keyListeners.push_back(KeyListener(this, KeyCode::MOUSE_LEFT,       GLFW_MOUSE_BUTTON_LEFT,     KeyDevice::MOUSE));
  keyListeners.push_back(KeyListener(this, KeyCode::MOUSE_RIGHT,      GLFW_MOUSE_BUTTON_RIGHT,    KeyDevice::MOUSE));

  staticEventListener.listeners.push_back(this);
  glfwSetScrollCallback(window, [](GLFWwindow* window, double xoffset, double yoffset) {
    staticEventListener.notifyScroll(yoffset);
  });
}

void Window::clearKeyListeners() {
  keyListeners.remove_if([&](const KeyListener & l) {
    return l.window==this;
  });
}

void Window::processKeys(float deltaTime) {
  for(auto & l : keyListeners) {
    l.listen(deltaTime);
  }

  if(glfwGetWindowAttrib(window, GLFW_HOVERED)) {
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if(xmouse!=x || ymouse!=y) {
      xmouse = x;
      ymouse = y;
      onMouseMove(xmouse, ymouse);
    }
  }
}
