#include "AppWindow.hpp"

void AppWindow::afterInit() {

#ifdef TYPE_PARAMETRIC_SURFACE
  initCubicSplinePatch();
#else
  initImplicit();
#endif

}

void AppWindow::render(float deltaTime) {
  glClearColor(0.2, 0.2, 0.2, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  if(surface) renderImplicit();
  if(cubicSplinePatch) renderCubicSplinePatch();
}

void AppWindow::renderImGui(float deltaTime) {
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  // Start the Dear ImGui frame
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();

  ImGui::Begin(name.c_str());

  if(ImGui::RadioButton("WIRES", drawWire)) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    drawWire = true;
  }
  else if(ImGui::RadioButton("FILLED", !drawWire)) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    drawWire = false;
  }

  renderCameraImGui();
  renderImplicitImgui();
  renderCubicSplinePatchImGui();

  ImGui::End();

  ImGui::Render();
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
  {
    GLFWwindow* backup_current_context = glfwGetCurrentContext();
    ImGui::UpdatePlatformWindows();
    ImGui::RenderPlatformWindowsDefault();
    glfwMakeContextCurrent(backup_current_context);
  }
}

void AppWindow::onKeyDown(KeyCode key, KeyDevice device) {
  if(key==KeyCode::UP) {
    
  }
  if(key==KeyCode::DOWN) {
    
  }
  if(key==KeyCode::RIGHT) {
    
  }
  if(key==KeyCode::LEFT) {
    
  }
}

void AppWindow::onKeyPush(KeyCode key, KeyDevice device) {
  if(key == KeyCode::A) {
    
  }
  else if(key == KeyCode::X) {
    
  }
}

void AppWindow::onMouseMove(float x, float y) {
  lastMousePos = {x,y};
}

void AppWindow::onMouseScroll(float y) {
  
}
