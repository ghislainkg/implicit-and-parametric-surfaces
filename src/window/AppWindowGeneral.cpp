#include "AppWindow.hpp"

void AppWindow::renderCameraImGui() {
  float saveCursorPosX = ImGui::GetCursorPosX();
  // Rotate camera up
  ImGui::SetCursorPosX(saveCursorPosX+50);
  ImGui::Button("Rotate /\\");
  if(ImGui::IsItemActive()) {
    if(surface) {
      glm::vec3 p = tracer.getCamera().getPos();
      auto axis = glm::normalize(glm::cross(tracer.getCamera().getDirection(), glm::vec3(0,1,0)));
      p = glm::rotate(glm::mat4(1), -0.03f, axis)*glm::vec4(p, 1.0);
      tracer.getCamera().setPos(p);
      tracer.getCamera().setDirection(-p);
      updateImplicitTracerFrame();
    }
    if(cubicSplinePatch) {
      glm::vec3 p = camera.pos;
      auto axis = glm::normalize(glm::cross(camera.getFront(), camera.up));
      p = glm::rotate(glm::mat4(1), -0.03f, axis)*glm::vec4(p, 1.0);
      camera.pos = p;
    }
  }
  // Rotate camera left
  ImGui::Button("Rotate <-");
  if(ImGui::IsItemActive()) {
    if(surface) {
      glm::vec3 p = tracer.getCamera().getPos();
      p = glm::rotate(glm::mat4(1), -0.03f, glm::vec3(0,1,0))*glm::vec4(p, 1.0);
      tracer.getCamera().setPos(p);
      tracer.getCamera().setDirection(-p);
      updateImplicitTracerFrame();
    }
    if(cubicSplinePatch) {
      glm::vec3 p = camera.pos;
      p = glm::rotate(glm::mat4(1), -0.03f, glm::vec3(0,1,0))*glm::vec4(p, 1.0);
      camera.pos = p;
    }
  }
  ImGui::SameLine();
  // Rotate camera right
  ImGui::Button("Rotate ->");
  if(ImGui::IsItemActive()) {
    if(surface) {
      glm::vec3 p = tracer.getCamera().getPos();
      p = glm::rotate(glm::mat4(1), 0.03f, glm::vec3(0,1,0))*glm::vec4(p, 1.0);
      tracer.getCamera().setPos(p);
      tracer.getCamera().setDirection(-p);
      updateImplicitTracerFrame();
    }
    if(cubicSplinePatch) {
      glm::vec3 p = camera.pos;
      p = glm::rotate(glm::mat4(1), 0.03f, glm::vec3(0,1,0))*glm::vec4(p, 1.0);
      camera.pos = p;
    }
  }
  // Rotate camera down
  ImGui::SetCursorPosX(saveCursorPosX+50);
  ImGui::Button("Rotate \\/");
  if(ImGui::IsItemActive()) {
    if(surface) {
      glm::vec3 p = tracer.getCamera().getPos();
      auto axis = glm::normalize(glm::cross(tracer.getCamera().getDirection(), glm::vec3(0,1,0)));
      p = glm::rotate(glm::mat4(1), 0.03f, axis)*glm::vec4(p, 1.0);
      tracer.getCamera().setPos(p);
      tracer.getCamera().setDirection(-p);
      updateImplicitTracerFrame();
    }
    if(cubicSplinePatch) {
      glm::vec3 p = camera.pos;
      auto axis = glm::normalize(glm::cross(camera.getFront(), camera.up));
      p = glm::rotate(glm::mat4(1), 0.03f, axis)*glm::vec4(p, 1.0);
      camera.pos = p;
    }
  }
  ImGui::Separator();
}
