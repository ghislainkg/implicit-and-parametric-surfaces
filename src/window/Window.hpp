#ifndef _GUM_IMPLICIT_SURF_WINDOW_
#define _GUM_IMPLICIT_SURF_WINDOW_

#include "../Utils.hpp"

enum KeyDevice {
  KEYBOARD, MOUSE
};

enum KeyCode {
  UP, DOWN, LEFT, RIGHT,
  Q, W, E, A, S, D, Z, X, C,
  SPACE, CTRL_LEFT, CTRL_RIGHT,
  MOUSE_LEFT, MOUSE_RIGHT
};

class Window {
  friend class KeyListener;
  friend class EventListener;
public:
  Window(
    int width, int height, std::string name
  ): width(width), height(height), name(name) {}

  inline void init() {
    initGLFW();
    initOpenGL();
    initImGui();
    afterInit();
  }
  inline void end() {
    clearImGui();
    endWindow();
  }

  void loop();

protected:
  GLFWwindow * window;
  int width;
  int height;
  std::string name;

  virtual void afterInit() {};
  virtual void render(float deltaTime) {};
  virtual void renderImGui(float deltaTime) {};

  virtual void onKeyDown(KeyCode key, KeyDevice device) {};
  virtual void onKeyPush(KeyCode key, KeyDevice device) {};

  virtual void onMouseMove(float x, float y) {};
  virtual void onMouseScroll(float y) {};

private:
  float lastTime = 0;

  float keyPushDelay = 0.5f; // in seconds
  void initKeyListeners();
  void clearKeyListeners();
  void processKeys(float deltaTime);

  void initGLFW();
  void initOpenGL();
  void initImGui();

  void clearImGui();
  void endWindow();

  double xmouse, ymouse;
};

#endif
