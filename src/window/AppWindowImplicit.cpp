#include "AppWindow.hpp"

void AppWindow::initImplicit() {
  implicit_program.initProgram(
    {
      std::string(GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR)+
      std::string("/texture-render/vertex.glsl")
    },
    {
      std::string(GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR)+
      std::string("/texture-render/fragment.glsl")
    },
    {}
  );

#ifdef TYPE_SKELETON_SURFACE
  // Point surface
  PointImplicitSurface * s = new PointImplicitSurface();
  s->setIsoValue(0.70);
  s->points.push_back({0,0,-1.0});
  s->points.push_back({0.0,0,1.0});
  surface = s;
#else
#ifdef TYPE_POLYNOMIAL_SURFACE
  // Polynomial surface
  PolynomialImplicitSurface * s = new PolynomialImplicitSurface();
  surface = s;
#endif
#endif

  tracer.surface = surface;
  tracer.getCamera().setPos({1, 0, 0});
  tracer.getCamera().setDirection({-1, 0, 0});
  tracer.computeFrame();

  // Init texture for tracer frame
  glGenTextures(1, &implicit_textureID);
  glBindTexture(GL_TEXTURE_2D, implicit_textureID);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGB,
    tracer.getCamera().getW(), tracer.getCamera().getH()
    , 0, GL_RGB, GL_FLOAT, tracer.getPixelsColor().data());
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  getOpenGLError("Texture init");

  // Init quad for texture rendering
  glGenVertexArrays(1, &implicit_VAO);
  glBindVertexArray(implicit_VAO);
  std::vector<glm::vec2> vertices = {
    {-1, 1}, {1,-1}, {1, 1},
    {-1, 1}, {-1,-1}, {1, -1},};
  std::vector<glm::vec2> texCoords = {
    {0, 1}, {1,0}, {1, 1},
    {0, 1}, {0,0}, {1, 0},};
  glGenBuffers(1, &implicit_vertices_VBO);
  glGenBuffers(1, &implicit_texCoords_VBO);
  // Vertices
  glBindBuffer(GL_ARRAY_BUFFER, implicit_vertices_VBO);
  glBufferData(GL_ARRAY_BUFFER,
    vertices.size()*sizeof(float)*2, vertices.data(),
    GL_DYNAMIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
  glEnableVertexAttribArray(0);
  getOpenGLError("Vertices data");
  // Tex coordinates
  glBindBuffer(GL_ARRAY_BUFFER, implicit_texCoords_VBO);
  glBufferData(GL_ARRAY_BUFFER,
    texCoords.size()*sizeof(float)*2, texCoords.data(),
    GL_DYNAMIC_DRAW);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
  glEnableVertexAttribArray(1);
  getOpenGLError("tex coords data");
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

void AppWindow::updateImplicitTracerFrame() {
  std::cout << "update" << std::endl;
  tracer.computeFrame();

  glBindTexture(GL_TEXTURE_2D, implicit_textureID);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGB,
    tracer.getCamera().getW(), tracer.getCamera().getH()
    , 0, GL_RGB, GL_FLOAT, tracer.getPixelsColor().data());
}

void AppWindow::renderImplicit() {
  implicit_program.use();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, implicit_textureID);
  glUniform1i(
    glGetUniformLocation(implicit_program.program, "imageTexture"),
    0
  );
  getOpenGLError("texture uniform");

  glBindVertexArray(implicit_VAO);
  glDrawArrays(GL_TRIANGLES, 0, 6);
  getOpenGLError("drawCall");
}

void AppWindow::renderImplicitImgui() {
  if(surface) {
// Color -----------------------------------------------
    if(ImGui::RadioButton("UNIFORM_COLOR", tracer.displayColor==DisplayColor::UNIFORM_COLOR)) {
      tracer.displayColor=DisplayColor::UNIFORM_COLOR;
      updateImplicitTracerFrame();
    }
    if(ImGui::RadioButton("CLOSEST_POINT_COLOR", tracer.displayColor==DisplayColor::CLOSEST_POINT_COLOR)) {
      tracer.displayColor=DisplayColor::CLOSEST_POINT_COLOR;
      updateImplicitTracerFrame();
    }
    if(ImGui::RadioButton("NORMAL_VEC", tracer.displayColor==DisplayColor::NORMAL_VEC)) {
      tracer.displayColor=DisplayColor::NORMAL_VEC;
      updateImplicitTracerFrame();
    }
    ImGui::Separator();

// Ray tracer camera --------------------------------
    if(ImGui::RadioButton("Orthographic", tracer.getCamera().projection==CameraProjection::ORTHOGRAPHIC)) {
      tracer.getCamera().projection=CameraProjection::ORTHOGRAPHIC;
      updateImplicitTracerFrame();
    }
    if(ImGui::RadioButton("Perspective", tracer.getCamera().projection==CameraProjection::PERSPECTIVE)) {
      tracer.getCamera().projection=CameraProjection::PERSPECTIVE;
      updateImplicitTracerFrame();
    }

    if(tracer.getCamera().projection==CameraProjection::ORTHOGRAPHIC) {
      float w = tracer.getCamera().orthoOpeningW;
      float h = tracer.getCamera().orthoOpeningH;
      ImGui::InputFloat("Width", &w, 0.01, 0.1);
      ImGui::InputFloat("Height", &h, 0.01, 0.1);
      if(w!=tracer.getCamera().orthoOpeningW || h!=tracer.getCamera().orthoOpeningH) {
        tracer.getCamera().orthoOpeningW = w;
        tracer.getCamera().orthoOpeningH = h;
        updateImplicitTracerFrame();
      }
    }
    if(tracer.getCamera().projection==CameraProjection::PERSPECTIVE) {
      float focal = tracer.getCamera().perspectiveFocalLength;
      ImGui::InputFloat("Focal length", &focal, 0.01f, 0.1f);
      if(focal != tracer.getCamera().perspectiveFocalLength) {
        tracer.getCamera().perspectiveFocalLength = focal;
        updateImplicitTracerFrame();
      }
    }

// Implicit shape modeling ------------------------------------------------------------
    auto isoVal = surface->getIsoValue();
    ImGui::InputFloat("IsoValue", &isoVal, 0.01f, 0.1f);
    if(isoVal != surface->getIsoValue()) {
      surface->setIsoValue(isoVal);
      updateImplicitTracerFrame();
    }
    ImGui::Separator();
    
    if(surface->getType() == POINT) {
      PointImplicitSurface * s = static_cast<PointImplicitSurface*>(surface);
      if(ImGui::RadioButton("BLENDING", s->assembling==BLENDING)) {
        s->assembling=BLENDING;
        updateImplicitTracerFrame();
      }
      if(ImGui::RadioButton("UNION", s->assembling==UNION)) {
        s->assembling=UNION;
        updateImplicitTracerFrame();
      }
      if(ImGui::RadioButton("INTERSECTION", s->assembling==INTERSECTION)) {
        s->assembling=INTERSECTION;
        updateImplicitTracerFrame();
      }

      if(ImGui::Button("Add point")) {
        s->points.push_back({0,0,0});
        selectedPointIndex = s->points.size()-1;
        updateImplicitTracerFrame();
      }
      if(ImGui::Button("Next point")) {
        selectedPointIndex = (selectedPointIndex+1)%s->points.size();
      }
      if(selectedPointIndex >= 0) {
        auto point = s->points.begin();
        std::advance(point, selectedPointIndex);
        glm::vec3 p = *point;
        ImGui::PushItemWidth(100);
        ImGui::InputFloat("x#implicit", &p.x, 0.01f, 0.1f);
        ImGui::SameLine();
        ImGui::InputFloat("y#implicit", &p.y, 0.01f, 0.1f);
        ImGui::SameLine();
        ImGui::InputFloat("z#implicit", &p.z, 0.01f, 0.1f);
        if(*point != p) {
          *point = p;
          updateImplicitTracerFrame();
        }
        ImGui::PopItemWidth();
      }
    }
    else if(surface->getType() == POLYNOMIAL) {
      PolynomialImplicitSurface * s = static_cast<PolynomialImplicitSurface*>(surface);
      ImGui::PushItemWidth(75);
      float saveCursorPosX = ImGui::GetCursorPosX();
      float newCursorPosX = saveCursorPosX+100;
      ImGui::SetCursorPosX(newCursorPosX);

      if(ImGui::Button("UPDATE")) {
        updateImplicitTracerFrame();
      }

      if(ImGui::Button("Add factor")) s->factors.push_back(
        {1.0, 0, 0, 0}
      );
      for(uint i=0; i<s->factors.size(); i++) {
        float coef = s->factors[i].coef;
        int degreeX = s->factors[i].degreeX;
        int degreeY = s->factors[i].degreeY;
        int degreeZ = s->factors[i].degreeZ;
        ImGui::InputFloat(
          std::string(std::string("Coef")+std::to_string(i)).c_str(), &coef, 0.1f, 1.0f);
          ImGui::SameLine();
        ImGui::InputInt(
          std::string(std::string("powX")+std::to_string(i)).c_str(), &degreeX, 1, 5);
          ImGui::SameLine();
        ImGui::InputInt(
          std::string(std::string("powY")+std::to_string(i)).c_str(), &degreeY, 1, 5);
          ImGui::SameLine();
        ImGui::InputInt(
          std::string(std::string("powZ")+std::to_string(i)).c_str(), &degreeZ, 1, 5);
        s->factors[i].coef = coef;
        s->factors[i].degreeX = degreeX;
        s->factors[i].degreeY = degreeY;
        s->factors[i].degreeZ = degreeZ;
      }

      ImGui::PopItemWidth();
      ImGui::SetCursorPosX(saveCursorPosX);
    }
  }
}
