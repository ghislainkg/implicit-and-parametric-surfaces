#include "AppWindow.hpp"

void AppWindow::initCubicSplinePatch() {

  // Initial cubic spline points and tangent planes
  cubicSplineProgram.initProgram(
    {std::string(GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR)+std::string("/cubic-spline-render/vertex.glsl")},
    {std::string(GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR)+std::string("/cubic-spline-render/fragment.glsl")},
    {std::string(GUM_IMPLICIT_SURF_SHADER_SOURCES_DIR)+std::string("/cubic-spline-render/geometry.glsl")}
  );
  cubicSplinePatch = new CubicSplineSurfacePatch();
  cubicSplinePatch->setPoint00({-1, 0, -1});
  cubicSplinePatch->setPoint01({-1, 0, 1});
  cubicSplinePatch->setPoint10({1, 0, -1});
  cubicSplinePatch->setPoint11({1, 0, 1});
  cubicSplinePatch->setTangentPlane00({-0.1, 5.5, 0.0}, {0.0, 0.5, -0.1});
  cubicSplinePatch->setTangentPlane01({-0.1, 0.5, 0.0}, {0.0, 0.5, 0.1});
  cubicSplinePatch->setTangentPlane10({0.1, 0.5, 0.0}, {0.0, 0.5, -0.1});
  cubicSplinePatch->setTangentPlane11({0.1, 0.5, 0.0}, {0.0, 0.5, 0.1});
  cubicSplinePatch->update();

  // Init VAO
  glGenVertexArrays(1, &cubicSplinePatchVAO);
  glBindVertexArray(cubicSplinePatchVAO);
  float splinePatch = 1.0;
  glGenBuffers(1, &cubicSplinePatch_VBO);
  glBindBuffer(GL_ARRAY_BUFFER, cubicSplinePatch_VBO);
  glBufferData(
    GL_ARRAY_BUFFER,
    sizeof(float),
    &splinePatch,
    GL_DYNAMIC_DRAW
  );
  glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), 0);
  glEnableVertexAttribArray(0);
  getOpenGLError("splinePatch data");
  glBindVertexArray(0);
}

static void uniformPoly(GLuint program, const std::string & name, std::vector<float> & poly) {
  for(uint i=0; i<4; i++) {
    for(uint j=0; j<4; j++) {
      uint index = 4*i + j;
      auto coefName = std::string(
        name+
        std::string("[")+std::to_string(index)+std::string("]")
      );
      glUniform1f(
        glGetUniformLocation(program, coefName.c_str()),
        poly[index]
      );
    }
  }
}
void AppWindow::renderCubicSplinePatch() {
  cubicSplineProgram.use();

  auto polyX = cubicSplinePatch->interpolationX.getPolynomialCoefs();
  auto polyY = cubicSplinePatch->interpolationY.getPolynomialCoefs();
  auto polyZ = cubicSplinePatch->interpolationZ.getPolynomialCoefs();
  uniformPoly(cubicSplineProgram.program, "polynomialCoefsX", polyX);
  uniformPoly(cubicSplineProgram.program, "polynomialCoefsY", polyY);
  uniformPoly(cubicSplineProgram.program, "polynomialCoefsZ", polyZ);

  glUniform1f(
    glGetUniformLocation(cubicSplineProgram.program, "resolutionU"),
    2
  );
  glUniform1f(
    glGetUniformLocation(cubicSplineProgram.program, "resolutionV"),
    2
  );

  glUniformMatrix4fv(
    glGetUniformLocation(cubicSplineProgram.program, "viewMat"), 1, GL_FALSE,
    glm::value_ptr(camera.getViewMatrix())
  );
  glUniformMatrix4fv(
    glGetUniformLocation(cubicSplineProgram.program, "projMat"), 1, GL_FALSE,
    glm::value_ptr(camera.getProjectionMatrix())
  );

  glBindVertexArray(cubicSplinePatchVAO);
  glDrawArrays(GL_POINTS, 0, 1);
  getOpenGLError("drawCall");
}

void AppWindow::renderCubicSplinePatchImGui() {
  if(cubicSplinePatch) {
    // Point 00
    auto p00 = cubicSplinePatch->getPoint00();
    ImGui::Text("P00");
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("x#p00", &p00.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("y#p00", &p00.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("z#p00", &p00.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setPoint00(p00);
    // Point 01
    ImGui::Text("P01");
    auto p01 = cubicSplinePatch->getPoint01();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("x#p01", &p01.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("y#p01", &p01.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("z#p01", &p01.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setPoint01(p01);
    // Point 10
    ImGui::Text("P10");
    auto p10 = cubicSplinePatch->getPoint10();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("x#p10", &p10.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("y#p10", &p10.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("z#p10", &p10.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setPoint10(p10);
    // Point 11
    ImGui::Text("P11");
    auto p11 = cubicSplinePatch->getPoint11();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("x#p11", &p11.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("y#p11", &p11.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("z#p11", &p11.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setPoint11(p11);

    // Tangent plane 00
    ImGui::Text("Tangent plane 00");
    auto tan00 = cubicSplinePatch->getTangentPlane00();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("v.x#tan00", &tan00.first.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.y#tan00", &tan00.first.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.z#tan00", &tan00.first.z, 0.01f, 0.1f);
    ImGui::InputFloat("u.x#tan00", &tan00.second.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.y#tan00", &tan00.second.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.z#tan00", &tan00.second.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setTangentPlane00(tan00.first, tan00.second);
    // Tangent plane 01
    ImGui::Text("Tangent plane 01");
    auto tan01 = cubicSplinePatch->getTangentPlane01();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("v.x#tan01", &tan01.first.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.y#tan01", &tan01.first.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.z#tan01", &tan01.first.z, 0.01f, 0.1f);
    ImGui::InputFloat("u.x#tan01", &tan01.second.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.y#tan01", &tan01.second.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.z#tan01", &tan01.second.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setTangentPlane01(tan01.first, tan01.second);
    // Tangent plane 10
    ImGui::Text("Tangent plane 10");
    auto tan10 = cubicSplinePatch->getTangentPlane10();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("v.x#tan10", &tan10.first.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.y#tan10", &tan10.first.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.z#tan10", &tan10.first.z, 0.01f, 0.1f);
    ImGui::InputFloat("u.x#tan10", &tan10.second.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.y#tan10", &tan10.second.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.z#tan10", &tan10.second.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setTangentPlane10(tan10.first, tan10.second);
    // Tangent plane 11
    ImGui::Text("Tangent plane 11");
    auto tan11 = cubicSplinePatch->getTangentPlane11();
    ImGui::PushItemWidth(100);
    ImGui::InputFloat("v.x#tan11", &tan11.first.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.y#tan11", &tan11.first.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("v.z#tan11", &tan11.first.z, 0.01f, 0.1f);
    ImGui::InputFloat("u.x#tan11", &tan11.second.x, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.y#tan11", &tan11.second.y, 0.01f, 0.1f);
    ImGui::SameLine();
    ImGui::InputFloat("u.z#tan11", &tan11.second.z, 0.01f, 0.1f);
    ImGui::PopItemWidth();
    cubicSplinePatch->setTangentPlane11(tan11.first, tan11.second);

    cubicSplinePatch->update();
  }
}
