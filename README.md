# Exploration/Demo of implicit and explicit surface representations

### Implicit surface representation

* Skeleton based implicit modeling
![alt text](images/skeleton.png "Point implicit surface")

* Basic implicit primitives from polynomial functions
![alt text](images/polynomial.png "Polynomial implicit surface")

They are implemented in the *./src/surfaces/* folder.

They are displayed through direct raytracing (*./src/raytracing*).

### Parametric surface representation:

* Simple Bicubic Spline Surface patch
![alt text](images/cubicspline.png "Cubic Spline surface patch")

They are implemented in the *./src/parametric/* folder.

They are displayed by generating points in geometry shader (Not the best solution since the number of vertices returned by the shader is limited).

### Build

* Create a *./build/* directory.
* go to *./build/* directory.
* Commands :

```bash
# for parametric surface demo
cmake -D DBUILD_PARAMETRIC=ON ..
# for skeleton-based implicit surface demo
cmake -D DBUILD_SKELETON=ON ..
# for polynomial implicit surface demo
cmake -D DBUILD_POLYNOMIAL=ON ..

# Then
cmake --build . 
```

### Third party libraries

* [Glad](https://github.com/Dav1dde/glad) for OpenGL calls. (MIT Licence)
* [GLM](https://glm.g-truc.net/) for maths with OpenGL. (MIT Licence)
* [GLFW](https://www.glfw.org/) for OpenGL context and window system. (zlib/libpng License, a BSD-like license)
* [STB](https://github.com/nothings/stb) for image loading and writing. (MIT Licence)
* [Dear ImGUI](https://github.com/ocornut/imgui) for the GUI. (MIT Licence)
* [Eigen](https://eigen.tuxfamily.org/) Large matrix operations. (MPL2-licensed)
